variable "region" {
  description = "Value of the regions"
  type        = string
  default     = "eu-central-1"
}

variable "instance_type" {
  description = "Value of the regions"
  type        = string
  default     = "t2.micro"
}

variable "allowed_cidr_blocks" {
  type    = list(any)
  default = ["0.0.0.0/0"]
}

variable "availability_zones" {
  type    = list(any)
  default = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
}

variable "database_name" {
  description = "Name of the database"
  type        = string
  default     = "myassessmentdb"
}

variable "database_user" {
  description = "User of the database"
  type        = string
  default     = "admin"
}

variable "database_password" {
  description = "Password of the Database"
  type        = string
  default     = "admin123"
}

variable "amis" {
  type = map(any)
  default = {
    "eu-central-1" = "ami-01d9d7f15bbea00b7"
    #"eu-central-1" = "ami-0844eaeefba93f1e0"
  }
}

variable "instance_name" {
  description = "Name of the instance"
  type        = string
  default     = "My-PA-EC2-Instance-webserver"
}

variable "endpoint" {
  description = "Value of the endpoing"
  type        = string
  default     = "my-pa-endpoint"
}
