provider "aws" {
  region = "eu-central-1"
}

data "aws_caller_identity" "this" {
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.24.1"
    }
  }
  required_version = ">= 0.14.2"
}