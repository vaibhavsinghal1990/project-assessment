#### CREATE RDS PRIVATE  SUBNET
resource "aws_subnet" "rds_private_subnet1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = false
  availability_zone       = var.availability_zones[0]

  tags = {
    Name        = "my_pa_rds_private_subnet1"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}

resource "aws_subnet" "rds_private_subnet2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = false
  ##### here avl_zone is taken from variable "var.availability_zones" and [x] ####denotes index position.
  availability_zone = var.availability_zones[1]

  tags = {
    Name        = "my_pa_rds_private_subnet2"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}

####CREATE SUBNET GROUP####
resource "aws_db_subnet_group" "rds" {
  name       = "my_pa_rds"
  subnet_ids = ["${aws_subnet.rds_private_subnet1.id}", "${aws_subnet.rds_private_subnet2.id}"]

  tags = {
    Name        = "my_pa_rds"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}

######CREATE RDS SECURITY GROUP

resource "aws_security_group" "rds" {
  name        = "my-pa-mysql-allow-securitygroup"
  description = "ssh allow to the mysql"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description     = "ssh"
    security_groups = ["${aws_security_group.web_sg1.id}", "${aws_security_group.web_sg2.id}"]
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
  }

  ingress {
    description     = "MYSQL"
    security_groups = ["${aws_security_group.web_sg1.id}", "${aws_security_group.web_sg2.id}"]
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "my_pa_msql_allow_securitygroup"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}

#### RDS DB OPTION GROUP####
resource "aws_db_option_group" "rds" {
  name                     = "my-pa-optiongroup-test"
  option_group_description = "Terraform Option Group for promotion assessment"
  engine_name              = "mysql"
  major_engine_version     = "5.7"

  option {
    option_name = "MARIADB_AUDIT_PLUGIN"

    option_settings {
      name  = "SERVER_AUDIT_EVENTS"
      value = "CONNECT"
    }

    option_settings {
      name  = "SERVER_AUDIT_FILE_ROTATIONS"
      value = "37"
    }
  }
}

### CREATE DB PARAMETER GROUP

resource "aws_db_parameter_group" "rds" {
  name   = "my-pa-rdsmysql"
  family = "mysql5.7"

  parameter {
    name  = "autocommit"
    value = "1"
  }

  parameter {
    name  = "binlog_error_action"
    value = "IGNORE_ERROR"
  }
}

##### CREATE RDS DB INSTANCE######

resource "aws_db_instance" "rds" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "5.7.19"
  instance_class         = "db.t2.micro"
  name                   = var.database_name
  username               = var.database_user
  password               = var.database_password
  db_subnet_group_name   = aws_db_subnet_group.rds.id
  option_group_name      = aws_db_option_group.rds.id
  publicly_accessible    = "false"
  vpc_security_group_ids = ["${aws_security_group.rds.id}"]
  parameter_group_name   = aws_db_parameter_group.rds.id
  skip_final_snapshot    = true


  tags = {
    Name        = "my_pa_rds-mysql_database"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}
