####CREATE EC2 INSTANCE
resource "aws_instance" "app_server" {
  ami                                  = var.amis[var.region]
  instance_type                        = "t2.micro"
  associate_public_ip_address          = true
  key_name                             = "my-pa-key"
  vpc_security_group_ids               = ["${aws_security_group.web_sg1.id}", "${aws_security_group.web_sg2.id}"]
  subnet_id                            = aws_subnet.web_subnet1.id
  user_data                            = templatefile("user_data.tfpl", { rds_endpoint = "${aws_db_instance.rds.endpoint}", user = var.database_user, password = var.database_password, dbname = var.database_name })
  instance_initiated_shutdown_behavior = "terminate"
  root_block_device {
    volume_type = "gp2"
    volume_size = "15"
  }
  provisioner "local-exec" {
    command = "sleep 120"
  }
  tags = {
    Name        = var.instance_name
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }

  depends_on = [aws_db_instance.rds]
}

###### CREATE EC2 IMAGE #########

resource "aws_ami_from_instance" "ec2_image" {
  name               = "my-pa-web-insance"
  source_instance_id = aws_instance.app_server.id

  depends_on = [aws_instance.app_server]
  tags = {
    Name        = "my-pa-ami"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}

####### CREATE AUTO SCALING LAUNCH COINFIG #######

resource "aws_launch_configuration" "ec2" {
  image_id        = aws_ami_from_instance.ec2_image.id
  instance_type   = "t2.micro"
  key_name        = "my-pa-key"
  security_groups = ["${aws_security_group.web_sg1.id}", "${aws_security_group.web_sg2.id}"]
  lifecycle {
    create_before_destroy = true
  }
}

## Creating AutoScaling Group#####
resource "aws_autoscaling_group" "ec2" {
  launch_configuration = aws_launch_configuration.ec2.id
  min_size             = 2
  max_size             = 3

  target_group_arns   = ["${aws_alb_target_group.group.arn}"]
  vpc_zone_identifier = ["${aws_subnet.web_subnet1.id}", "${aws_subnet.web_subnet2.id}"]
  health_check_type   = "EC2"

  tags = [
    {
      key   = "Project"
      value = "Promotion Assessment"
    },
    {
      key   = "Environment"
      value = "Prod"
    }
  ]
}
