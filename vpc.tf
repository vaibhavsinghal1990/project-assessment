# CREATE VPC ####

resource "aws_vpc" "vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = "true"
  enable_dns_support   = "true"

  tags = {
    Name        = "my_pa_vpc"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}

#### create IG ####


resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name        = "my_pa_internet_gateway"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}

#### ADD ROUTE TABLE TO IG ###

resource "aws_route" "route" {
  route_table_id         = aws_vpc.vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gateway.id
}
