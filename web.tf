#### CREATE  WEB SUBNET--PUBLIC#######
resource "aws_subnet" "web_subnet1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = true
  availability_zone       = var.availability_zones[1]

  tags = {
    Name        = "my_pa_web_public_subnet1"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}

resource "aws_subnet" "web_subnet2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "10.0.4.0/24"
  map_public_ip_on_launch = true
  availability_zone       = var.availability_zones[2]

  tags = {
    Name        = "my_pa_web_public_subnet2"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}

#CREATE  WEB SUCURITY GROUP
resource "aws_security_group" "web_sg1" {
  name        = "my_pa_web_securitygroup1"
  description = "Terraform my pa websecurity group 1"
  vpc_id      = aws_vpc.vpc.id
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "my_pa_web_security_group1"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}

#CREATE WEB SUCURITY GROUP2
resource "aws_security_group" "web_sg2" {
  name        = "my_pa_web_securitygroup2"
  description = "Terraform my pa websecurity group 2"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "my_pa_web_security_group2"
    Project     = "Promotion Assessment"
    Environment = "Prod"
  }
}

